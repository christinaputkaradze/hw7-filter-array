let arr = ['hello', 'world', 23, '23', null, false];
let typeStr = "string";
let typeNam = "number";
let typeBool = "boolean";

function filterBy(arr, type){
    // виріант 1
    let filteredArray = arr.filter(item => typeof item !== type);
    
    // варіант 2
    // let filteredArray = [];

    // arr.forEach((item) => {
    //         if (typeof item !== type) {
    //                 filteredArray.push(item);
    //         }
    // } )
  
    return filteredArray;
}

let resultStr = filterBy(arr, typeStr);
console.log(resultStr);
let resultNam = filterBy(arr, typeNam);
console.log(resultNam);
let resultBool = filterBy(arr, typeBool);
console.log(resultBool);